import React from "react";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

type PropType = {
  children?: any;
};

export const ColumnWrapper: React.FC<PropType> = (props: PropType) => {
  const { children } = props;

  return <DndProvider backend={HTML5Backend}>{children}</DndProvider>;
};

export default (ColumnWrapper);
