import React from "react";
import { ItemTypes } from "../../constants/index";
import { useDrag, DragSourceMonitor } from "react-dnd";
import { arrangeLists } from "../../views/DashBoard/dashboardAction";
import { Dispatch } from "redux";
import { connect } from "react-redux";

const style: React.CSSProperties = {
  float: "left",
};

export interface BoxProps {
  children: any;
  colID: number;
  arrangeLists: (x: any) => void;
}

interface DropResult {
  allowedDropEffect: string;
  dropEffect: string;
  targetID: number;
}

interface DragItem {
  colID: number;
  type: string;
}

let Box: React.FC<BoxProps> = ({ children, colID, arrangeLists }) => {
  const item = { colID: colID, type: ItemTypes.BOX };
  const [{ opacity }, drag] = useDrag({
    item,
    end(item: DragItem | undefined, monitor: DragSourceMonitor) {
      const dropResult: DropResult = monitor.getDropResult();
      if (item && dropResult) {
        console.log(item, dropResult);
        arrangeLists({ from: item.colID, to: dropResult.targetID });
      }
    },
    collect: (monitor: any) => ({
      opacity: monitor.isDragging() ? 0.4 : 1,
    }),
  });

  return (
    <div ref={drag} style={{ ...style, opacity }}>
      {children}
    </div>
  );
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  arrangeLists: (x: any) => dispatch(arrangeLists(x)),
});

export default connect(null, mapDispatchToProps)(Box);
