import React from "react";
import { useDrop } from "react-dnd";
import { ItemTypes } from "../../constants/index";

export interface DustbinProps {
  allowedDropEffect: string;
  children: any;
  id: number;
}

function selectBackgroundColor(isActive: boolean, canDrop: boolean) {
  if (isActive) {
    return "darkgreen";
  } else if (canDrop) {
    return "darkkhaki";
  } else {
    return "#222";
  }
}

export const Dustbin: React.FC<DustbinProps> = ({
  allowedDropEffect,
  children,
  id,
}) => {
  const [{ canDrop, isOver }, drop] = useDrop({
    accept: ItemTypes.BOX,
    drop: () => ({
      targetID: id,
      allowedDropEffect,
    }),
    collect: (monitor: any) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });

  const isActive = canDrop && isOver;
  const backgroundColor = selectBackgroundColor(isActive, canDrop);
  return (
    <div ref={drop} style={{ backgroundColor }}>
      {children}
    </div>
  );
};
