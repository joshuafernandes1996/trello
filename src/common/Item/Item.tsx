import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Card, IconButton } from "@material-ui/core";
import Background from "../../assets/bg.jpg";
import Subject from "@material-ui/icons/Subject";
import Modal from "../Modal/Modal";
import Edit from "@material-ui/icons/Create";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
      backgroundImage: `url(${Background})`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      height: "100%",
      margin: 8,
      cursor: "pointer",
    },
    title: {
      fontWeight: 700,
      textAlign: "center",
      width: "100%",
      fontFamily: `"Comic Sans MS", cursive, sans-serif`,
      opacity: 0.5,
    },
    paper: {
      padding: 8,
      display: "grid",
      minHeight: 50,
    },
  })
);

type PropType = {
  todo: string;
  children?: any;
  description?: string;
  title: string;
  id: number;
  colID: number;
};

export const Item: React.FC<PropType> = (props: PropType) => {
  const classes = useStyles();
  const { todo, children, description, title, id, colID } = props;

  const [data, setData] = React.useState({
    isModalOpen: false,
    isHovering: false,
  });

  const handleOpenModal = () => {
    setData({ ...data, isHovering: false, isModalOpen: true });
  };

  const handleCloseModal = () => {
    setData({ ...data, isModalOpen: false });
  };

  const setHoverTrue = () => {
    setData({ ...data, isHovering: true });
  };
  const setHoverFalse = () => {
    setData({ ...data, isHovering: false });
  };

  return (
      <div
        className={classes.grow}
        onMouseOver={() => setHoverTrue()}
        onMouseLeave={() => setHoverFalse()}
      >
        <Card
          onClick={() => !children && handleOpenModal()}
          className={classes.paper}
        >
          {children ? (
            children
          ) : (
            <div>
              <div style={{ position: "relative" }}>
                {todo}
                {data.isHovering && (
                  <IconButton
                    aria-label="show more"
                    aria-haspopup="true"
                    color="inherit"
                    style={{
                      right: 0,
                      position: "absolute",
                    }}
                  >
                    <Edit />
                  </IconButton>
                )}
              </div>
              {description && <Subject />}
            </div>
          )}
        </Card>
        <Modal
          listID={colID}
          cardID={id}
          isOpen={data.isModalOpen}
          cardName={todo}
          colName={title}
          description={description}
          handleConfirm={handleCloseModal}
          handleCancel={handleCloseModal}
        />
      </div>
  );
};

export default (Item);
