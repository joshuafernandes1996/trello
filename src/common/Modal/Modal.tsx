import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  TextField,
  IconButton,
} from "@material-ui/core";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import Background from "../../assets/bg.jpg";
import VideoLabel from "@material-ui/icons/VideoLabel";
import Subject from "@material-ui/icons/Subject";
import Clear from "@material-ui/icons/Clear";
import { addDesc } from "../../views/DashBoard/dashboardAction";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
      backgroundImage: `url(${Background})`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      height: "100%",
      margin: 8,
    },
    title: {
      fontWeight: 700,
      textAlign: "center",
      width: "100%",
      fontFamily: `"Comic Sans MS", cursive, sans-serif`,
      opacity: 0.5,
      "&::before": {
        content: '"\\E910"',
      },
    },
    paper: {
      padding: 8,
      display: "grid",
    },
    dialogPaper: {
      width: 500,
      backgroundColor: "#f4f5f7",
    },
    multilineColor: {
      backgroundColor: "#fff",
    },
    mainTitle: {
      fontSize: "20px",
      fontWeight: "bold",
      marginLeft: 16,
      color: "#172b4d",
    },
    subTitle: {
      fontSize: "14px",
      marginLeft: 40,
      color: "#5e6c84",
    },
    desc: {
      fontSize: "20px",
      fontWeight: "bold",
      marginLeft: 16,
      color: "#172b4d",
    },
  })
);

type PropType = {
  listID: number;
  cardID: number;
  isOpen: boolean;
  cardName: string;
  colName: string;
  description: string | undefined;
  children?: any;
  handleConfirm: (e?: any) => void;
  handleCancel: (e?: any) => void;
  addDesc: (x: any) => void;
};

export const Modal: React.FC<PropType> = (props: PropType) => {
  const classes = useStyles();

  const {
    listID,
    cardID,
    cardName,
    colName,
    description,
    isOpen,
    handleCancel,
    children,
    addDesc,
  } = props;

  const [data, setData] = React.useState({ desc: description, isDirty: false });

  const handleChange = (
    e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    setData({ ...data, desc: e.target.value, isDirty: true });
  };

  const handleSave = () => {
    addDesc({ listID: listID, cardID: cardID, description: data.desc });
    setData({ ...data, isDirty: false });
    handleCancel();
  };

  const handleCloseModal = () => {
    if (data.isDirty) {
      if (
        // eslint-disable-next-line no-restricted-globals
        confirm("Do you want to Save Changes?")
      ) {
        handleSave();
      } else {
        setData({ ...data, desc: description, isDirty: false });
        handleCancel();
      }
    } else {
      setData({ ...data, desc: description, isDirty: false });
      handleCancel();
    }
  };

  return (
    <Dialog
      open={isOpen}
      onClose={handleCancel}
      classes={{ paper: classes.dialogPaper }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          <div style={{ display: "flex", alignItems: "center" }}>
            <VideoLabel /> <div className={classes.mainTitle}>{cardName}</div>
          </div>
          <div className={classes.subTitle}>
            in List <u>{colName}</u>
          </div>
        </DialogContentText>
        {children && children}
        <div style={{ display: "flex", alignItems: "center", marginTop: 32 }}>
          <Subject /> <div className={classes.desc}>Description</div>
        </div>
        <div
          style={{
            marginLeft: 40,
            marginTop: 8,
          }}
        >
          <TextField
            id="outlined-basic"
            fullWidth
            value={data.desc}
            label="Add a more detailed Description"
            InputProps={{
              className: classes.multilineColor,
            }}
            multiline
            rows={5}
            variant="filled"
            onChange={(e) => handleChange(e)}
          />
        </div>
      </DialogContent>
      <DialogActions
        style={{ textAlign: "left", display: "block", paddingLeft: 64 }}
      >
        <Button
          style={{ backgroundColor: "#5aac44", color: "#fff" }}
          onClick={() => handleSave()}
          variant="contained"
          color="primary"
        >
          Save
        </Button>
        <IconButton
          aria-label="show more"
          aria-haspopup="true"
          color="inherit"
          onClick={() => handleCloseModal()}
        >
          <Clear />
        </IconButton>
      </DialogActions>
    </Dialog>
  );
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  addDesc: (x: any) => dispatch(addDesc(x)),
});

export default connect(null, mapDispatchToProps)(Modal);
