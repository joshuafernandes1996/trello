import React from "react";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import { IAppState } from "../../store/types";
import Card from "../Card/Card";
import update from "immutability-helper";
import Item from "../Item/Item";
import { arrangeCards } from "../../views/DashBoard/dashboardAction";

type PropType = {
  items: any;
  colID: number;
  title: string;
  arrangeCards: (x: any) => void;
};

export const CardItem: React.FC<PropType> = (props: PropType) => {
  const { items, colID, title, arrangeCards } = props;

  const [cards, setCards] = React.useState(items);

  React.useEffect(() => {
    setCards(items);
  }, [items]);

  const moveCard = React.useCallback(
    (dragIndex: number, hoverIndex: number) => {
      const dragCard = cards[dragIndex];
      setCards(
        update(cards, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        })
      );
      arrangeCards({
        colID: colID,
        items: update(cards, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        }),
      });
    },
    [arrangeCards, cards, colID]
  );

  const renderCard = (
    card: { id: number; todo: string; description: string },
    index: number
  ) => {
    return (
      <Card key={card.id} index={index} id={card.id} moveCard={moveCard}>
        <Item
          colID={colID}
          title={title}
          key={index}
          todo={card.todo}
          description={card.description}
          id={card.id}
        />
      </Card>
    );
  };

  return (
    <>
      <div style={{ width: "100%" }}>
        {cards.map(
          (
            card: { id: number; todo: string; description: string },
            i: number
          ) => renderCard(card, i)
        )}
      </div>
    </>
  );
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  arrangeCards: (x: any) => dispatch(arrangeCards(x)),
});

export const mapStateToProps = (state: IAppState) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(CardItem);
