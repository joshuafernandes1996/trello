import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import {
  Paper,
  IconButton,
  Typography,
  Button,
  Card,
  TextField,
  ButtonBase,
  Menu,
  MenuItem,
} from "@material-ui/core";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import Background from "../../assets/bg.jpg";
import MoreIcon from "@material-ui/icons/MoreHoriz";
import Item from "../Item/Item";
import Add from "@material-ui/icons/Add";
import Clear from "@material-ui/icons/Clear";
import Arrow from "@material-ui/icons/ArrowBackIos";
import { addCard, sortCards } from "../../views/DashBoard/dashboardAction";
import ColumnDNDWrapper from "../DNDWrappers/ColumnWrapper";
import CardItems from "../Card/CardItem";
import ColAsBox from "../DNDWrappers/ColAsBoxWrapper";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
      backgroundImage: `url(${Background})`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      height: "100%",
    },
    addBtn: {
      flexGrow: 1,
    },
    title: {
      fontWeight: 700,
      textAlign: "center",
      width: "100%",
      fontFamily: `"Comic Sans MS", cursive, sans-serif`,
      opacity: 0.5,
    },
    paper: {
      width: 300,
      height: 550,
      backgroundColor: "#ebecf0",
    },
    mainContainer: {
      position: "relative",
      height: 48,
      top: 8,
      marginLeft: 8,
    },
    card: {
      display: "flex",
      alignItems: "center",
      width: "100%",
      padding: 8,
      color: "#172b4d",
      backgroundColor: "rgba(0,0,0,.08)",
    },
  })
);
interface Item {
  todo: string;
}

type PropType = {
  id: number;
  items: Item[];
  title: string;
  addCard: (x: any) => void;
  sortCards: (x: any) => void;
};

export const Column: React.FC<PropType> = (props: PropType) => {
  const classes = useStyles();
  const { id, items, title, addCard, sortCards } = props;

  const [data, setData] = React.useState({
    isAddNewCardClicked: false,
    items: items,
    newCardTitle: "",
  });

  const handleAddNew = () => {
    setData({
      ...data,
      isAddNewCardClicked: true,
    });
  };

  const handleAddNewCol = () => {
    if (data.newCardTitle.length === 0) {
      alert("Please Enter Card Name.");
      return;
    }
    addCard({ newCard: data.newCardTitle, id: id });
    setData({
      ...data,
      isAddNewCardClicked: false,
    });
  };

  const handleChange = (
    e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    setData({ ...data, newCardTitle: e.target.value });
  };

  const handleCancel = () => {
    setData({
      ...data,
      isAddNewCardClicked: false,
    });
  };

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <ColAsBox colID={id}>
      <div className={classes.grow}>
        <Paper className={classes.paper}>
          <div className={classes.mainContainer}>
            <div style={{ position: "absolute", top: 0, left: 8 }}>
              <Typography variant="h6" noWrap style={{ fontWeight: "bold" }}>
                {title}
              </Typography>
            </div>
            <div style={{ position: "absolute", top: 0, right: 8 }}>
              <IconButton
                aria-label="show more"
                aria-haspopup="true"
                color="inherit"
                onClick={(e) => handleClick(e)}
              >
                <MoreIcon />
              </IconButton>
            </div>
          </div>
          <div style={{ maxHeight: 480, overflowY: "auto" }}>
            <ColumnDNDWrapper>
              <CardItems colID={id} title={title} items={items} />
              <Item
                colID={id}
                title={title}
                key={"testAdd"}
                todo={""}
                id={987987}
              >
                {!data.isAddNewCardClicked && (
                  <ButtonBase
                    className={classes.addBtn}
                    onClick={() => handleAddNew()}
                  >
                    <Card className={classes.card}>
                      <Add />
                      Add Another Item
                    </Card>
                  </ButtonBase>
                )}
                {data.isAddNewCardClicked && (
                  <Card style={{ backgroundColor: "#ebecf0" }}>
                    <div style={{ margin: 8 }}>
                      <div>
                        <TextField
                          label={"Enter List Title..."}
                          style={{ backgroundColor: "#fff" }}
                          onChange={(e) => {
                            handleChange(e);
                          }}
                        />
                      </div>
                      <div>
                        <Button
                          style={{ backgroundColor: "#5aac44", color: "#fff" }}
                          type={"submit"}
                          onClick={() => handleAddNewCol()}
                        >
                          Add List
                        </Button>
                        <IconButton
                          aria-label="show more"
                          aria-haspopup="true"
                          color="inherit"
                          onClick={() => handleCancel()}
                        >
                          <Clear />
                        </IconButton>
                      </div>
                    </div>
                  </Card>
                )}
              </Item>
            </ColumnDNDWrapper>
          </div>
        </Paper>
      </div>
      {/* Menu For the sorting Options */}
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>
          <div style={{ display: "flex", alignItems: "center" }}>
            <div key={1}>
              <IconButton
                aria-label="show more"
                aria-haspopup="true"
                color="inherit"
                onClick={() => handleClose()}
              >
                <Arrow />
              </IconButton>
            </div>
            <div key={2}>{"Sort List"}</div>
            <div key={3}>
              <IconButton
                aria-label="show more"
                aria-haspopup="true"
                color="inherit"
                onClick={() => handleClose()}
              >
                <Clear />
              </IconButton>
            </div>
          </div>
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleClose();
            sortCards({ type: "new", colID: id });
          }}
        >
          Date Created (Newest First)
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleClose();
            sortCards({ type: "old", colID: id });
          }}
        >
          Date Created (Oldest First)
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleClose();
            sortCards({ type: "alpha", colID: id });
          }}
        >
          Card Name (Alphabetically)
        </MenuItem>
      </Menu>
    </ColAsBox>
  );
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  addCard: (x: any) => dispatch(addCard(x)),
  sortCards: (x: any) => dispatch(sortCards(x)),
});

export default connect(null, mapDispatchToProps)(Column);
