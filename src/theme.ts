/*Theme Configuration File */
import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({
    palette: {
        primary: { main: '#00B0F0' },
    },
    typography: {
        fontFamily: `'Quicksand', sans-serif`,
    },
    overrides: {
        MuiButton: {
            root: {
                textTransform: 'none',
            },
        },
    },
});
