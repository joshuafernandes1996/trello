import { combineReducers } from "redux";
import { dashboardReducer } from "../views/DashBoard/dashboardReducer";

const appReducer = combineReducers({
  dashboardDetails: dashboardReducer,
});

const rootReducer = (state: any, action: any) => {
  return appReducer(state, action);
};

export default rootReducer;
