/* eslint-disable @typescript-eslint/no-explicit-any */
import { applyMiddleware, createStore, compose } from "redux";
import rootReducer from "./rootReducer";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const globalAny: any = global;

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const persistConfig = {
  key: "root",
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const middleware: any[] = [];
const { createLogger } = require("redux-logger");
const invariant = require("redux-immutable-state-invariant").default;

middleware.push(invariant());
middleware.push(createLogger({ collapsed: true }));

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const configStore = (initialState = {}) => {
  const store = createStore(
    persistedReducer,
    initialState as any,
    composeEnhancer(applyMiddleware(...middleware))
  );

  return {
    persistor: persistStore(store),
    store,
  };
};

const { store, persistor } = configStore();

globalAny.store = store;

export { store, persistor };
