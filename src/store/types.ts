export interface ICommonReducerState {
  isLoading: boolean;
}

export interface listItem {
  id: number;
  title: string;
  items: any;
}

export interface IDReducerState {
  list: listItem[];
  sortOrder: boolean;
}

export interface IAppState {
  readonly commonDetails: ICommonReducerState;
  readonly dashboardDetails: IDReducerState;
}
