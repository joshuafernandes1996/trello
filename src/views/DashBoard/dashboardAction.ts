import { createAction } from 'redux-actions';
import { dashboardActionTypes } from '../../constants/index';

export const addList = createAction(dashboardActionTypes.ADD_LIST);
export const addCard = createAction(dashboardActionTypes.ADD_CARD);
export const addDesc = createAction(dashboardActionTypes.ADD_DESC);
export const arrangeCards = createAction(dashboardActionTypes.ARR_CARDS);
export const arrangeLists = createAction(dashboardActionTypes.ARR_LISTS);
export const sortCards = createAction(dashboardActionTypes.SORT);