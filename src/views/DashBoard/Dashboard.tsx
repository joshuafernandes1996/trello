import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import {
  Grid,
  Card,
  ButtonBase,
  TextField,
  Button,
  IconButton,
  GridList,
} from "@material-ui/core";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import { IAppState, listItem } from "../../store/types";
import Background from "../../assets/bg.jpg";
import Column from "../../common/Column/Column";
import Add from "@material-ui/icons/Add";
import Clear from "@material-ui/icons/Clear";
import { addList } from "./dashboardAction";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { Dustbin as ListWrapper } from "../../common/DNDWrappers/ListWrapper";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
      backgroundImage: `url(${Background})`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      height: "100%",
      padding: 16,
    },
    title: {
      fontWeight: 700,
      textAlign: "center",
      width: "100%",
      fontFamily: `"Comic Sans MS", cursive, sans-serif`,
      opacity: 0.5,
    },
    root: {
      flexGrow: 1,
    },
    paper: {
      width: 200,
      height: 200,
    },
    gridList: {
      flexWrap: "nowrap",
      height: "86vh",
      // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
      transform: "translateZ(0)",
    },
  })
);

type PropType = {
  list: listItem[];
  addList: (x: string) => void;
};

export const Dashboard: React.FC<PropType> = (props: PropType) => {
  const classes = useStyles();
  const { list, addList } = props;

  console.log(list);

  const [data, setData] = React.useState({
    isAddClicked: false,
    newItemItem: "",
  });
  
  const handleAddNew = () => {
    setData({ ...data, isAddClicked: true });
  };

  const handleCancel = () => {
    setData({ ...data, isAddClicked: false });
  };

  const handleAddNewCol = () => {
    if (data.newItemItem.length === 0) {
      alert("Please Give a Title");
      return;
    }
    addList(data.newItemItem);
    setData({ ...data, isAddClicked: false });
  };

  const handleChange = (
    e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    setData({ ...data, newItemItem: e.target.value });
  };

  return (
    <div className={classes.grow}>
      <Grid container className={classes.root} spacing={2}>
        <Grid item xs={12}>
          <Grid container justify="flex-start" spacing={2}>
            <GridList className={classes.gridList} cols={0}>
              {list.map((value, i) => (
                <Grid key={i} item style={{ padding: 8 }}>
                  <DndProvider backend={HTML5Backend}>
                    <ListWrapper id={value.id} allowedDropEffect="move">
                      <Column
                        title={value.title}
                        items={value.items}
                        id={value.id}
                      />
                    </ListWrapper>
                  </DndProvider>
                </Grid>
              ))}
              <Grid key={"testUD"} item style={{ padding: 8 }}>
                {!data.isAddClicked && (
                  <ButtonBase onClick={() => handleAddNew()}>
                    <Card
                      style={{
                        display: "flex",
                        alignItems: "center",
                        padding: 8,
                        color: "#172b4d",
                        backgroundColor: "rgba(0,0,0,.08)",
                      }}
                    >
                      <Add />
                      Add Another Item
                    </Card>
                  </ButtonBase>
                )}
                {data.isAddClicked && (
                  <Card style={{ backgroundColor: "#ebecf0" }}>
                    <div style={{ margin: 8 }}>
                      <div>
                        <TextField
                          label={"Enter List Title..."}
                          style={{ backgroundColor: "#fff" }}
                          onChange={(e) => {
                            handleChange(e);
                          }}
                        />
                      </div>
                      <div>
                        <Button
                          style={{ backgroundColor: "#5aac44", color: "#fff" }}
                          onClick={() => handleAddNewCol()}
                        >
                          Add List
                        </Button>
                        <IconButton
                          aria-label="show more"
                          aria-haspopup="true"
                          color="inherit"
                          onClick={() => handleCancel()}
                        >
                          <Clear />
                        </IconButton>
                      </div>
                    </div>
                  </Card>
                )}
              </Grid>
            </GridList>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  addList: (x: string) => dispatch(addList(x)),
});

export const mapStateToProps = (state: IAppState) => {
  return {
    list: state.dashboardDetails.list,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
