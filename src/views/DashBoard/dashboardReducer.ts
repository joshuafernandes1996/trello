import { dashboardActionTypes } from "../../constants/index";
import { Reducer } from "redux";
import { IDReducerState } from "../../store/types";

export const initialState: IDReducerState = {
  list: [
    {
      id: 123,
      title: "title",
      items: [
        {
          id: 1,
          todo: "testA",
          description: "random1",
          timeStamp: "Mar 12 2012 10:00:00 AM",
        },
        {
          id: 2,
          todo: "testB",
          description: "random2",
          timeStamp: "Mar 12 2012 8:00:00 AM",
        },
        {
          id: 3,
          todo: "testC",
          description: "",
          timeStamp: "Mar 12 2012 6:00:00 AM",
        },
        {
          id: 4,
          todo: "testZ",
          description: "random4",
          timeStamp: "Mar 12 2012 4:00:00 AM",
        },
        {
          id: 5,
          todo: "testX",
          description: "",
          timeStamp: "Mar 12 2012 2:00:00 AM",
        },
        {
          id: 6,
          todo: "testT",
          description: "random6",
          timeStamp: "Mar 12 2012 1:00:00 AM",
        },
      ],
    },
  ],
  sortOrder: false,
};

const reducer: Reducer<IDReducerState> = (state = initialState, action) => {
  switch (action.type) {
    case dashboardActionTypes.ADD_LIST: {
      return {
        ...state,
        list: [
          ...state.list,
          {
            id: Math.floor(Math.random() * 1000),
            title: action.payload,
            items: [],
          },
        ],
      };
    }
    case dashboardActionTypes.ADD_CARD: {
      const index = state.list.findIndex(
        (post) => post.id === action.payload.id
      );

      return {
        ...state,
        list: [
          ...state.list.slice(0, index), // everything before current post
          {
            ...state.list[index],
            items: [
              ...state.list[index].items,
              {
                id: Math.floor(Math.random() * 1000),
                todo: action.payload.newCard,
                description: "",
              },
            ],
          },
          ...state.list.slice(index + 1), // everything after current post
        ],
      };
    }
    case dashboardActionTypes.ADD_DESC: {
      const listIndex = state.list.findIndex(
        (post) => post.id === action.payload.listID
      );

      const cardIndex = state.list[listIndex].items.findIndex(
        (post: { id: number }) => post.id === action.payload.cardID
      );

      let newList = [
        ...state.list.slice(0, listIndex),
        {
          ...state.list[listIndex],
          items: [
            ...state.list[listIndex].items.slice(0, cardIndex),
            {
              ...state.list[listIndex].items[cardIndex],
              description: action.payload.description,
            },
            ...state.list[listIndex].items.slice(cardIndex + 1),
          ],
        },
        ...state.list.slice(listIndex + 1),
      ];

      return {
        ...state,
        list: newList,
      };
    }
    case dashboardActionTypes.ARR_CARDS: {
      let newArr = state.list.map((val) => {
        if (val.id === action.payload.colID) {
          return {
            ...val,
            items: action.payload.items,
          };
        } else return val;
      });
      return { ...state, list: newArr };
    }
    case dashboardActionTypes.ARR_LISTS: {
      const fromIndex = state.list.findIndex(
        (post) => post.id === action.payload.from
      );

      const toIndex = state.list.findIndex(
        (post) => post.id === action.payload.to
      );

      let newArr = SwapListItems(state.list, fromIndex, toIndex);

      return { ...state, list: newArr };
    }
    case dashboardActionTypes.SORT: {
      const index = state.list.findIndex(
        (post) => post.id === action.payload.colID
      );

      let sortedItems = sort(state.list, action.payload.type, index);

      let newArr = state.list.map((val, i) => {
        if (i === index) {
          return {
            ...val,
            items:
              state.sortOrder && action.payload.type === "alpha"
                ? sortedItems.reverse()
                : sortedItems,
          };
        } else return val;
      });

      return { ...state, list: newArr, sortOrder: !state.sortOrder };
    }
    default: {
      return state;
    }
  }
};

//should swap/rearrange columns without mutation erros
function SwapListItems(items: any[], firstIndex: number, secondIndex: number) {
  const results = items.slice();
  const firstItem = items[firstIndex];
  results[firstIndex] = items[secondIndex];
  results[secondIndex] = firstItem;

  return results;
}

function sort(items: any[], type: string, index: number) {
  const results = items.slice();
  let cards = results[index].items.slice();

  switch (type) {
    case "new": {
      cards = cards
        .sort((a: { timeStamp: string }, b: { timeStamp: string }) => {
          let c = new Date(a.timeStamp);
          let d = new Date(b.timeStamp);
          return c.valueOf() - d.valueOf();
        })
        .reverse();
      break;
    }
    case "old": {
      cards = cards.sort(
        (a: { timeStamp: string }, b: { timeStamp: string }) => {
          let c = new Date(a.timeStamp);
          let d = new Date(b.timeStamp);
          return c.valueOf() - d.valueOf();
        }
      );
      break;
    }
    case "alpha": {
      cards = cards.sort((a: { todo: string }, b: { todo: any }) =>
        a.todo.localeCompare(b.todo)
      );
      break;
    }
    default:
      break;
  }

  return cards;
}

export { reducer as dashboardReducer };