import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { AppBar, Toolbar, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
    },
    title: {
      fontWeight: 700,
      textAlign: "center",
      width: '100%',
      fontFamily: `"Comic Sans MS", cursive, sans-serif`,
      opacity: 0.5,
    },
  })
);

type PropType = {

};

export const HeaderAppBar: React.FC<PropType> = (props: PropType) => {
  const classes = useStyles();

  return (
    <div className={classes.grow}>
      <AppBar
        style={{ color: "#fff", backgroundColor: "#172b4d" }}
        position="fixed"
      >
        <Toolbar>
          <Typography className={classes.title} variant="h6" noWrap>
            Trello
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default (HeaderAppBar);
