import keyMirror from "fbjs/lib/keyMirror";

export const commonActionTypes = keyMirror({
  SHOW_PROGRESS: undefined,
  HIDE_PROGRESS: undefined,
});

export const ItemTypes = {
  CARD: "card",
  BOX: 'box',
};


export const dashboardActionTypes = keyMirror({
  ADD_LIST: undefined,
  ADD_CARD: undefined,
  ADD_DESC: undefined,
  ARR_CARDS: undefined,
  ARR_LISTS: undefined,
  SORT: undefined,
});
