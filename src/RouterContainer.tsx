import React, { Suspense, Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  withRouter,
} from "react-router-dom";
import { RouteComponentProps } from "react-router";
import { connect } from "react-redux";
import Header from "./views/Header/Header";
import Dashboard from "./views/DashBoard/Dashboard";
import { IAppState } from "./store/types";

interface PropType extends RouteComponentProps {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  user?: any;
}

export class RouterContainer extends Component<PropType> {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  render() {
    let routes = null;
    let content = null;

    console.log(this.props.user);

    if (false) {
      //notAuthenticated
      routes = (
        <Switch>
          <Route path="/board" exact component={Dashboard} />
          <Redirect to="/board" />
        </Switch>
      );
      content = <div>{routes}</div>;
    } else {
      routes = (
        <Switch>
          <Route path="/board" exact component={Dashboard} />
          <Redirect to="/board" />
        </Switch>
      );

      content = (
        <div
          style={{
            height: "100%",
            overflowY: "hidden",
            overflowX: "scroll",
            marginTop: 64,
          }}
        >
          <Suspense fallback={<div />}>{routes}</Suspense>
        </div>
      );
    }
    return (
      <Router>
        <div id="main" style={{ height: "100%" }}>
          <Header />
          {content}
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state: IAppState) {
  return {
    user: null,
  };
}

export default connect(mapStateToProps, null)(withRouter(RouterContainer));
