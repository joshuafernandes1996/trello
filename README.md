# Trello Demo App

## Here are a few libraries and packagaes I used to get this App Done
 - Material-UI - `For UI Components`
 - Redux - `to handle State Management`
 - ReduxLogger - `Debugging tools`
 - ReduxPersists - `Helps Persist Data on refresh`
 - ReactDND - `Library to help Drag and Drop/ Rearrages elements`

## Hosting in S3 Bucket (Had to Strike this out since s3 only deals with static data)
 - I have hosted the Project on AWS S3 bucket
 - Created a Gitlab CI/CD Pipeline to auto deploy on S3 once merged to Master
 - Pipeline has Two Stage Build and Deploy
 - It will only deploy if anything is Pushed to Master, other branches it will only build and keep the artifact

## Link to Project Hosted on EC2 Instance of AWS
Click Here [Trello Demo App](http://ec2-13-232-99-236.ap-south-1.compute.amazonaws.com/)
 
## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
